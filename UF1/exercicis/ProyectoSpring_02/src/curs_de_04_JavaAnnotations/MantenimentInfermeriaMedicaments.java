package curs_de_04_JavaAnnotations;

import org.springframework.stereotype.Component;

@Component("mantInferMedica")

public class MantenimentInfermeriaMedicaments implements MantenimentInfermeriaInterface {

	@Override
	public String ferMantenimentInfermeria() {
		// TODO Auto-generated method stub
		return "ferMantenimentInfermeria(): manteniments dels medicaments de l'infermeria.";		
	}

}
