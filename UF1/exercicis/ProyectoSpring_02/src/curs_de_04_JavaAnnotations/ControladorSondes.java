package curs_de_04_JavaAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;



@Component("controladorSondesNau")
public class ControladorSondes implements Tripulants {
	
	// attributes
	public String nom;
	
	public ControladorSondes() {
	}
	
	//s'executara despres d'haver creat el mbean (simula el init())
	@PostConstruct
	public void inicialitzador() {
		System.out.println("ControladorSOndes: S'HA EXECUTAT inicialitzador() AMB LA JAVA ANNOTATION @PostConstruct");
	// si retornes alguna altra cossa que void seria dificil caputara lo que retorni
		// millor que no retorni res, postcontruct no pot rebre cap arguments
		this.nom = "Patata";
		System.out.println(this.nom);
	}
	@PreDestroy
	public void finzalitzador() {
		System.out.println("ControladorSondes: S'HA EXECUTAT finalitzador() AMB LA JAVA ANNOTATION @PReDestroy.");
		this.nom = "Voladora";
		System.out.println(this.nom);
	}
	
	
	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "controladorSondes: agafarTarees().";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "controladorSOndesNAu: agafarInforme()";
	}

}
