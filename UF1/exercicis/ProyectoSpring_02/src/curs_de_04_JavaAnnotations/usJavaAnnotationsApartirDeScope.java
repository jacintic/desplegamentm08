package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotationsApartirDeScope {

	public static void main(String[] args) {
		System.out.println("-----------usJavaAnnotationsApartirDeScope - INICI -----------");
		System.out.println();
		
		System.out.println("ABANS DE CARREGAR EL CONTEXT");
		
		// Creem el contexte carregant el fitxer xml de configuració.
		// comment for @Configuration
		//ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Exercici.xml");
		
		// configuration lines
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		/*Peticio de beans del contenidor Spring.
		 * EL 1r parametre es el ID del bean que fara servir i que esta en
		 * aplicacioCOnfig.class (el ID es el nom del metode
		 * que porta la Java annotation@ bean que crea objectes de clase
		 * especialistaenecm java)*/
		
		Tripulants especialistaEnECM_1 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		// Trpultants especialistaEnECM_1 = contexte.getBean("especialistaEnECM",Tripultants.class)// fa lo mateix que la linea anterior
		
		System.out.println("Tripulants especialistaEnECM.agafarInforme():" + especialistaEnECM_1.agafarInforme());
		System.out.println("Tripulants especialistaEnECM.agafarTarees():" + especialistaEnECM_1.agafarTarees());
		
		System.out.println("DESPRES DE CARREGAR EL CONTEXT i ABANS DE CARREGAR EL BEAN.");
	
		
		// @Value
		EspecialistaEnECM especialistaEnECM_2 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		
		System.out.println("EspecialistaEnECM especialistaEnECM.agafarTarees():" + especialistaEnECM_2.agafarTarees());
		System.out.println("EspecialistaEnECM especialistaEnECM.getEmail():" + especialistaEnECM_2.getEmail());
		System.out.println("EspecialistaEnECM especialistaEnECM.getDepartamentNom():" + especialistaEnECM_2.getDepartamentNom());

		
		// autowired triple bean
		
		Tripulants especialistaEnECM_10 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		
		System.out.println("Tripulants especialistaEnECM.agafarInforme(): " + especialistaEnECM_10.agafarInforme());
		System.out.println("Tripulants especialistaEnECM.agafarTarees(): " + especialistaEnECM_10.agafarTarees());
		
		EspecialistaEnECM especialistaEnECM_20 = contexte.getBean("especialistaEnECMTripleInforme", EspecialistaEnECM.class);
		
		System.out.println("EspecialistaEnECM especialistaEnECMTripleINforme.imprimirTripleInforme(): " + especialistaEnECM_20.imprimirTripleInforme());
		System.out.println("EspecialistaEnECM especialistaEnECMTripleInforme.agafarTarees(): " + especialistaEnECM_20.agafarTarees());
		System.out.println("EspecialistaEnECM especialistaEnECM.getEmail():" + especialistaEnECM_20.getEmail());
		System.out.println("EspecialistaEnECM especialistaEnECM.getDepartamentNom():" + especialistaEnECM_20.getDepartamentNom());
		
		
		
		// INIT (PostConstruct) & DESTROY (PreDestroy) - INICI:
		//ControladorSondes controladorSondes = contexte.getBean("controladorSondesNau", ControladorSondes.class);
		
		System.out.println("DESPRES DE CARREGAR EL BEAN");
		
		//System.out.println(controladorSondes);
		
		// Tanquem el contexte (el fitxer xml).
		contexte.close();
		
		System.out.println();
		System.out.println("----------usJavaAnnotationsApartirDeScope - FI ----------");
	}

}
