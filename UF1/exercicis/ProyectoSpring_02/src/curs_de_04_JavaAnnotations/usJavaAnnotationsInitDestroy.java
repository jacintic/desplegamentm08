package curs_de_04_JavaAnnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotationsInitDestroy {

	public static void main(String[] args) {
		System.out.println("-----------usJavaAnnotationsApartirDeScope - INICI -----------");
		System.out.println();
		
		// Creem el contexte carregant el fitxer xml de configuració.
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Exercici.xml");
		
		// Demanem el bean al contenidor fent servir la java annotation.
		Tripulants doctor_1 = contexte.getBean("doctorNau", Doctor.class);
		Tripulants doctor_2 = contexte.getBean("doctorNau", Doctor.class);
		
		// comprovació metode per defecte (singleton)
		System.out.println("Patro SINGLETON");
		System.out.println("Dir de memoria de doctor_1" + doctor_1);
		System.out.println("Dir de memoria de doctor_2" + doctor_2);
		
		
		// peticio de beans al contenidor Spring
		// infermera
		//infermeraNau
		Tripulants infermera_1 = contexte.getBean("infermeraNau", Infermera.class);
		Tripulants infermera_2 = contexte.getBean("infermeraNau", Infermera.class);
		
		// comprovació metode prototype
		System.out.println("Patro PROTOTYPE");
		System.out.println("Dir de memoria de enfermera_1" + infermera_1);
		System.out.println("Dir de memoria de enfermera_2" + infermera_2);
		
	
		
		// Tanquem el contexte (el fitxer xml).
		contexte.close();
		
		System.out.println();
		System.out.println("----------usJavaAnnotationsApartirDeScope - FI ----------");
	}

}
