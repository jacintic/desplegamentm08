package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class EspecialistaEnECM implements Tripulants {
	
	// attributes
	InformeElectronicaInterface informeEspecialistaEnECM;
	
	// attributes @Value
	@Value("${emailDepartamentECM}")
	private String email;
	
	// attributes @autowired @bean
	private InformeElectronicaInterface informeEspecialistaEnESM;
	private InformeElectronicaInterface informeEspecialistaEnEPM;
		
	
	@Value("${departamentECMNom}")
	private String departamentNom;
	
	// constructor
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}
	
	// constructor @Bean @Autowired
	@Autowired
	public  EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM, InformeElectronicaInterface informeEspecialistaEnESM, InformeElectronicaInterface informeEspecialistaEnEPM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
		this.informeEspecialistaEnESM = informeEspecialistaEnESM;
		this.informeEspecialistaEnEPM = informeEspecialistaEnEPM;
	}
	
	// @bean @value @autowired
	public String imprimirTripleInforme() {
		String cadena = "EspecialistaEnECM: imprimirTripleInforme(): \n \t" + 
				"Informe ECM: " + informeEspecialistaEnECM.getInformeElectronica() + "\n\t" + 
				"Informe ESM" + informeEspecialistaEnESM.getInformeElectronica() + "\n\t" + 
				"Informe EPM: " + informeEspecialistaEnEPM.getInformeElectronica();
		return cadena;
	}
	
	
	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "EspecialisteEnECM: agafarTarees(): arreglar la electrònica d'infermeria.";
	}


	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return informeEspecialistaEnECM.getInformeElectronica();
	}

	public String getEmail() {
		return email;
	}

	public String getDepartamentNom() {
		return departamentNom;
	}
	
	/*
	@Override
	public String ferManteniment() {
		return null;
	}
	*/
}
