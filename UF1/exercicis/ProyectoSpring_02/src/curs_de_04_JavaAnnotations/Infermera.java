package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("infermeraNau")

@Scope("prototype")
public class Infermera implements Tripulants {
	
	//attributes from manteniment
	
	
	@Autowired	
	@Qualifier("mantenimentInfermeriaMaterial")
	private MantenimentInfermeriaMaterial mantenimentMaterial;
	
	@Autowired
	@Qualifier("mantInferMedica")
	private MantenimentInfermeriaInterface mantenimentMedicaments;
	
	
	// attribute
	private InformeInfermeriaInterface informeInfermera;
		@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "InfermeraNau: agafarTarees(): ajuda a curar als tripulants. ";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "infermeraNau: agafarInforme(): Informe de l'infermera de la missió." + informeInfermera.getInformeInfermeria();
	}
	@Autowired
	public void setInformeInfermera(InformeInfermeriaInterface informe) {
		this.informeInfermera = informe;
	}
	
	
	

}
