package curs_de_04_JavaAnnotations;

public class Capita implements Tripulants {
	
	// implement informe
	private InformeInterface informeNou;
	
	// declarem nou objecte informeINterface al constructor
	public Capita (InformeInterface inf) {
		this.informeNou = inf;
	}
	
	
	// tarees
	public String agafarTarees() {
		return "Aquesta son les tarees del capita";
	}
	// informe +
	@Override
	public String agafarInforme() {
		return "Informe del capita" + informeNou.getInforme();
	}
}
