package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("doctorNau")

public class Doctor implements Tripulants {

	// attributes from informeinfermeria
	private InformeInfermeriaInterface informeDoctor;
	
	
	@Autowired
	public Doctor(InformeInfermeriaInterface informeDoctor) {
		this.informeDoctor = informeDoctor;
	}

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "doctorNau: agafarTarees(): curar als tripulants. ";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "doctorNau: agafarInforme(): Informe del doctor de la missió. " + informeDoctor.getInformeInfermeria();
	}

}
