package curs_de_04_JavaAnnotations;

import org.springframework.stereotype.Component;

@Component

public class InformeDoctor implements InformeInfermeriaInterface {

	@Override
	public String getInformeInfermeria() {
		// TODO Auto-generated method stub
		return "Informe d'infermeria generat pel doctor (generat per InformeDoctor.java)";
	}

}
