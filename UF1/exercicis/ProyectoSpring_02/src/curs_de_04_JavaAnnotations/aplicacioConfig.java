package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration

@ComponentScan("curs_de_04_JavaAnnotations")

//Propertysource/value
@PropertySource("classpath:dadesNau.properties")

public class aplicacioConfig {

	// Definim el bean per a InformeECM.java
	// L'identificador d'aquest bean serà el nom del mètode (infromeECM()).
	// Podem posar el nom que volguem al mètode.
	@Bean 
	public InformeElectronicaInterface informeECM() {
		return new InformeECM();
	}
	
	/*Degfinim el bean per a EspecialisteEnECM.java i a més
	 * l'injectem les dependències (en aquest cas
	 * l'injectem 1 objecte del tipus InformeElectronicaInterface)
	 * L'identificador d'aquest bean sera el nom de metode (especialistaEnECM())
	 *  i la dependencia injectada sera el bean informeECM().
	 *  La injecció es produex perque estem trucant al construcotr de la clase
	 *  EspecialistaEnECM ia alla te
	 *  com a parametre un objecte del tipus InformeIfermeriaInterface*/
	@Bean
	public Tripulants especialistaEnECM() {
		return new EspecialistaEnECM(informeECM());
	}
	
	// autowired bean
	@Bean 
	public InformeElectronicaInterface InformeESM() {
		return new InformeESM();
	}
	@Bean 
	public InformeElectronicaInterface InformeEPM() {
		return new InformeEPM();
	}
	// constructor
	@Bean
	public Tripulants especialistaEnECMTripleInforme() {
		return new EspecialistaEnECM(informeECM(), InformeESM(), InformeEPM() );
	}
	
	
	
}
