package anotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;



@Component

@Scope("prototype")
public class Dep_B_per_2 implements PersonalInterface {
	public int id;
	public String nom;
	
	protected Dep_B_per_2(int myId, String myName) {
		super();
		this.id = myId;
		this.nom = myName;
	}
	
	
	
	
	@Override
	public String toString() {
		return "Dep_B_per_2 [id=" + id + ", nom=" + nom + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	
	
	
	
}
