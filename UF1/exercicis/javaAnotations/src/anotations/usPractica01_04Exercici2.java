package anotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class usPractica01_04Exercici2 {

	public static void main(String[] args) {
			
		////////////INICI EXERCICI 2///////////
		// obrim anotations
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(practica01_04Config.class);
		
		
		// primer bean, departament
		
		Departament d1 = contexte.getBean("dep-B", Departament.class);
		
		System.out.println(d1);
		
		//PersonalInterface cd = contexte.getBean("capDepartament-B", Dep_B_per_1.class);
		PersonalInterface cd = contexte.getBean("capDepartament", Dep_B_per_1.class);
		
		
		/*
		// llista departament
		ArrayList<Dep_B_per_2>  llistaMultiPersones = new ArrayList();
		//Dep_B_per_2 per1 = new Dep_B_per_2(1,"Paquito");
		//Dep_B_per_2 per2 = new Dep_B_per_2(2,"Manolo");
		Dep_B_per_2 per1 = new Dep_B_per_2(1,"Juan");
		Dep_B_per_2 per2 = new Dep_B_per_2(2,"Francis");
		llistaMultiPersones.add(per1);
		llistaMultiPersones.add(per2);
		
		// metode per a agregar persones a departament
		d1.addPersones(llistaMultiPersones);
		
		System.out.println();
		*/
		
		// organització i informe
		Organitzacio o_1 = contexte.getBean("organitzacio", Organitzacio.class);
		
		System.out.println("Organitzacio @Bean .toString() " + o_1);
		System.out.println("Contingut informe organitazcio per bean (o_1.informe.textAimprimirPerPantalla()) " + o_1.informe.textAimprimirPerPantalla());
		
		contexte.close();
	}

}
