package anotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;



@Configuration

@ComponentScan("anotations")

@PropertySource("classpath:practica01-04Departaments.propietats")

public class practica01_04Config {
	
	// bean informe
	@Bean
	public Informe informe() {
		return new Informe();
	}
	
	// bean Organitzacio
	@Bean
	public Organitzacio organitzacio() {
		return new Organitzacio(informe());
	}
}
