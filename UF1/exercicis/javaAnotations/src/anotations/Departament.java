package anotations;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("dep-B")

public class Departament {
	@Value("${iddept}")
	public int ID;
	@Value("${nomdept}")
	public String nom;
	@Value("${emaildept}")
	public String email;
	public Organitzacio organitzacio;
	public Dep_B_per_1 capDeDepartament;
	private final BeanFactory factory;
	//public BeanFactory factory;
	//public ArrayList<Dep_B_per_2> llistaPersonal;
	public List<Dep_B_per_2> llistaPersonal;
	public InformeDepartament informe;
	
	
	@Autowired
	public Departament(final BeanFactory f) {
		this.factory = f;
	}
	
	// metode agregar persones a la llista
	public void addPersones(List<Dep_B_per_2> someArrayList) {
		
		
		this.llistaPersonal = someArrayList.stream().map(param -> factory.getBean(Dep_B_per_2.class, param))
			.collect(Collectors.toList());
	}
	
	
	public int getID() {
		return ID;
	}



	public void setID(int iD) {
		ID = iD;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}


	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}


	// setter per a organitzacio
	@Autowired
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}



	public Dep_B_per_1 getCapDeDepartament() {
		return capDeDepartament;
	}



	public void setCapDeDepartament(Dep_B_per_1 capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}



	public List<Dep_B_per_2> getLlistaPersonal() {
		return llistaPersonal;
	}



	public void setLlistaPersonal(List<Dep_B_per_2> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}


	public InformeDepartament getInforme() {
		return informe;
	}



	public void setInforme(InformeDepartament informe) {
		this.informe = informe;
	}


	@Override
	public String toString() {
		return "Departament [ID=" + ID + ", nom=" + nom + ", email=" + email + ", organitzacio=" + organitzacio
				+ /*", capDeDepartament=" + capDeDepartament + ", llistaPersonal=" + llistaPersonal + */", informe="
				+ informe + "]";
	}
}
