package practica01_04;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("dep-B")

public class Departament {
	public int ID;
	public String nom;
	public String email;
	public Organitzacio organitzacio;
	public Personal capDeDepartament;
	public ArrayList<Personal> llistaPersonal;
	public InformeDepartament informe;
	
	
	
	public int getID() {
		return ID;
	}



	public void setID(int iD) {
		ID = iD;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}



	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}



	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}



	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}



	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}



	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}



	public InformeDepartament getInforme() {
		return informe;
	}



	public void setInforme(InformeDepartament informe) {
		this.informe = informe;
	}



	@Override
	public String toString() {
		return "Departament [ID=" + ID + ", nom=" + nom + ", email=" + email + ", organitzacio=" + organitzacio
				+ ", capDeDepartament=" + capDeDepartament + ", llistaPersonal=" + llistaPersonal + ", informe="
				+ informe + "]";
	}
}
