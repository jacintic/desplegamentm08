package practica01_04;

import org.springframework.context.support.ClassPathXmlApplicationContext;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class main {

	public static void main(String[] args) {
		// initialize reading from xml
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Practica01-04.xml");
		
		// create bean organització singleton
		Organitzacio o_1 = contexte.getBean("organitzacio",Organitzacio.class);
		Organitzacio o_2 = contexte.getBean("organitzacio",Organitzacio.class);
		// prova bean organitzacio
		o_1.nom = "Hola";
		
		System.out.println(o_1.nom);
		
		// comprovacio patro singleton
		System.out.println(o_1.equals(o_2));
		
		
		// creacio bean personal prototype
		
		Personal p_1 = contexte.getBean("dep-A-per-1",Personal.class);
		Personal p_2 = contexte.getBean("dep-A-per-1",Personal.class);
		
		// comprovació de personal patro prototype
		System.out.println(p_1.equals(p_2));
		// comprovacio de atributs
		System.out.println(p_1);
		
		
		// bean personal patro singleton
		
		Personal p_s1 = contexte.getBean("dep-A-per-2",Personal.class);
		Personal p_s2 = contexte.getBean("dep-A-per-2",Personal.class);
		
		// comprovació de personal patro prototype
		System.out.println(p_s1.equals(p_s2));
		
		// comprovacio de atributs
		System.out.println(p_s1);
		
		
		// bean cap departament personal singleton
		
		Personal pc_1 = contexte.getBean("capDeDepartament",Personal.class);
		
		System.out.println(pc_1);
		
		// departament
		Departament d_1 = contexte.getBean("departamentTipus1",Departament.class);
		System.out.println(d_1);
		
		// fi exercici 1
		contexte.close();
		
		
		
		
		
		
	}

}
