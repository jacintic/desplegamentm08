package practica01_04;

public class Organitzacio {
	public static final String ORG_CODE = "orga";
	public static int orgNum = 0;
	public String nom;
	public Informe informe;
	@Override
	public String toString() {
		return "Organitzacio [nom=" + nom + ", informe=" + informe + "]";
	}
	
}
