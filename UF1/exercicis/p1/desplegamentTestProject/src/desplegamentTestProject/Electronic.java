package desplegamentTestProject;

public class Electronic implements Tripulants{
	

	// implementant metode informe
	private InformeInterface InformeNou;
	
	public void setInformeNou(InformeInterface informeNou) {
		this.InformeNou = informeNou;
	}
	// nous atributs email i departament
	private String email;
	private String nomDepartament;
	
	
	
	@Override
	public String agafarTarees() {
		return "Sóc un dels electrònics.";
	}
	// modificat desde informe
	@Override
	public String agafarInforme() {
		return "Informe del tècnic en electronica." + InformeNou.getInforme();
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNomDepartament() {
		return nomDepartament;
	}
	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}
}
