package desplegamentTestProject;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usSingletonPrototype {

	public static void main(String[] args) {
		System.out.println("ABANS DE GARREGARE EL CONTEXT.");
		// creem el contexte carregant el fitxer xml de configuracio
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_SingletonPrototype.xml");
		System.out.println("DESPRES DE CARREGAR EL CONTEXT I ABANS DE CARREGAR EL BEAN");
		// peticio de bean al contenidor String
		Capita capita_1 = contexte.getBean("tripulantCapita",Capita.class);
		Capita capita_2 = contexte.getBean("tripulantCapita",Capita.class);
		System.out.println("Patro SINGLETON:");
		System.out.println("Dir. de memòria de capita_1: " + capita_1);
		System.out.println("Dir. de memòria de capita_2: " + capita_2);
		
		if (capita_1==capita_2) {
			System.out.print("capita_1 i capita_2 SI son el mateix objecte, per tant, apunten al mateix objecte.");
		} else {
			System.out.println("capita_1 i capita_2 NO son el matiex objecte per tant no apuntent al mateix objecte");
		}
		System.out.println();
		
		// electronic prototype
		Electronic electronic_1= contexte.getBean("tripulantElectronic",Electronic.class);
		Electronic electronic_2= contexte.getBean("tripulantElectronic",Electronic.class);
		System.out.println("Patro PROTOTYPE:");
		System.out.println("Dir. de memòria de electronic_1: " + electronic_1);
		System.out.println("Dir. de memòria de electronic_2: " + electronic_2);
		
		
		// electroinc prototype
		if (electronic_1==electronic_2) {
			System.out.print("electronic_1 i electronic_2 SI son el mateix objecte, per tant, apunten al mateix objecte.");
		} else {
			System.out.println("electronic_1 i electronic_2 NO son el matiex objecte per tant no apuntent al mateix objecte");
		}
		System.out.println();
		
		// navegant init destroy
		Navegant navegant_1= contexte.getBean("tripulantNavegant",Navegant.class);
		System.out.println("DESPRES DE CARREGAR EL BEAN.");
		System.out.println(navegant_1.agafarInforme());
		System.out.println("DESPRES D'EXECUTAR navegant.agafarTarees().");
		System.out.println("Patro BEAN INIT I DESTROY NAVEGANT:");
		
		
		// tanquem el contexte el fixer xml que ja no volem fer servir mes el seu contingut
		contexte.close();
		System.out.println("DESPRES DE TANCAR EL CONTEXT.");
	}

}
