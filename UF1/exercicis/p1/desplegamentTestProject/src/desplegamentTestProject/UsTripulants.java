package desplegamentTestProject;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsTripulants {

	public static void main(String[] args) {
		/*Tripulants capita = new Capita();
		Tripulants maquinista = new Maquinista();
		Tripulants electronic = new Electronic();
		
		System.out.println(capita.agafarTarees());
		System.out.println(maquinista.agafarTarees());
		System.out.println(electronic.agafarTarees());		
		*/
		//Creamos el contexto cargando el fichero xml de configuacion.
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Creamos objeto haciendo servir el bean
		
		Tripulants capita = contexte.getBean("tripulantCapita", Tripulants.class);
		System.out.println(capita.agafarTarees());
		
		// nou de capita informe
		System.out.println(capita.agafarInforme());
		
		Tripulants maquinista = contexte.getBean("tripulantMaquinista", Tripulants.class);
		System.out.println(maquinista.agafarTarees());
		// nou tripulant per properties
		Maquinista maquinista2 = contexte.getBean("tripulantMaquinista", Maquinista.class);
		System.out.println("Email" + maquinista2.getEmail());
		System.out.println("Departament" + maquinista2.getNomDepartament());
		
		Tripulants electronic = contexte.getBean("tripulantElectronic", Tripulants.class);
		System.out.println(electronic.agafarTarees());
		
		// nou d'electronic
		System.out.println(electronic.agafarInforme());
		// nou electronic mail i dept
		Electronic electronic_2 = contexte.getBean("tripulantElectronic", Electronic.class);
		System.out.println("Email" + electronic_2.getEmail());
		System.out.println("Departament" + electronic_2.getNomDepartament());
		contexte.close();	
				
	}
}
