package desplegamentTestProject;

public class Navegant implements Tripulants {
	// atributs
	private InformeInterface informeNou;
	// setter
	public void setInformeNou(InformeInterface informeNou) {
		this.informeNou = informeNou;
	}
	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "agafarTarees(): Soc el navegant de la nau Leonov";
	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "agafarInforme(): Informe del navegant." + informeNou.getInforme();
	}
	
	// metode init
	public void metodeInit() {
		System.out.println("Acabem de llançar el metode init() de la classe Navegant.class just abans de que el bean estigui llest.");
	}
	// metode destroy
	public void metodeDestroy() {
		System.out.println("Acabem de llançar el metode destroy() de la calsse Navegant.java just despés de que el bean hagi estat destruit (hagi mort)");
	}
}
