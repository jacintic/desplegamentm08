package desplegamentTestProject;

public class Maquinista implements Tripulants{
	
	private String email;
	private String nomDepartament;
	
	public String agafarTarees() {
	return "Aquesta son les tarees del Maquinista";
	}
	public String agafarInforme() {
		return "Informe del Maquinista";
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNomDepartament() {
		return nomDepartament;
	}
	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}
}
