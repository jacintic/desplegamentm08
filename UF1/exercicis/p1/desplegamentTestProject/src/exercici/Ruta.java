/**
 * 
 */
package exercici;

import java.util.ArrayList;
import java.time.LocalDate;

/**
 * @author iaw14270791
 * @nameAuthor Jacint IC
 *
 */
public class Ruta {
	private String id;
	private String nom;
	private Waypoint waypoint;
	private ArrayList<Waypoint> llistaWaypoints;
	private LocalDate dataCreacio;
	public Ruta() {
	}
	public Ruta(String id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	// creem setter de Waypoint
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Waypoint getWaypoint() {
		return waypoint;
	}
	public void setWaypoint(Waypoint waypoint) {
		this.waypoint = waypoint;
	}
	public ArrayList<Waypoint> getLlistaWaypoints() {
		return llistaWaypoints;
	}
	public void setLlistaWaypoints(ArrayList<Waypoint> llistaWaypoints) {
		this.llistaWaypoints = llistaWaypoints;
	}
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	public String test() {
		return "test";
	}
	// creem un getter de waypoint
	
}
