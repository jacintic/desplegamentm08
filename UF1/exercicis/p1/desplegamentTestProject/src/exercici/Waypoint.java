/**
 * 
 */
package exercici;

import java.time.LocalDate;
/**
 * @author iaw14270791
 * @nameAuthor Jacint IC
 */
public class Waypoint {
	private int id;
	private String nom;
	private LocalDate dataCreacio;
	// fem el getter de Waypoint
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	
	public String test() {
		return "test";
	}
}
