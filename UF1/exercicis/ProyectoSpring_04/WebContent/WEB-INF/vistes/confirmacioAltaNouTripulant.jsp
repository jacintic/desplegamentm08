<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmacio d'alta d'un nou tripulant - CCCP LEonov</title>
</head>
<body>
	S'ha donat d'alta el tripulant <b> ${el_doctor.nom} ${el_doctor.cognom} </b>
	<br>
	El departament del nou tripulant és <b> ${el_doctor.departament}</b>
	<br>
	<br>
	Els conneixements del nou tripulant són <b> ${el_doctor.conneixements}</b>
	<br>
	<br>
	La ciutat de naixement del nou tripulant és <b> ${el_doctor.ciutatNaixement}</b>
	<br>
	<br>
	Els idiomes que sap el nou tripulant són <b> ${el_doctor.idiomes}</b>
	<br>
	<br>
	L'edat del tripulant es <b> ${el_doctor.edat}</b>
	<br>
	<br>
	L'email del tripulant es <b> ${el_doctor.email}</b>
	<br>
	<br>
	El codi postal del nou tripulant es <b> ${el_doctor.codiPostal}</b>
	<br>
	<br>
	El telèfon del nou tripulant es <b> ${el_doctor.telefon}</b>
	
	
</body>
</html>