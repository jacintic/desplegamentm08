package curs_de_05_AplicacionesWeb;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import curs_de_05_AplicacionesWeb.validacionsPersonalitzades.telefonFinlandia;

public class Tripulant {
	// attributes
	// validation hibernate
	@NotNull
	@Size(min = 4, message = "Long. minima de nom ha de ser 4 caracters.")
	private String nom;
	private String cognom;
	// attributes validate hibernate, mail and age
	@NotNull
	@Min(value = 18, message = "El nou tripulant ha de tenir com a minim 18 anys.")
	@Max(value = 65, message = "El nou tripulant ha de tenir com a maxim 65 anys.")
	private int edat;
	@NotBlank(message = "S'ha d'omplir l'email.")
	@Email
	private String email;
	// new attribute lista desplegable
	private String departament;
	// select multiple
	private String conneixements;
	// radio buttons and checkbox
	private String ciutatNaixement;
	private String idiomes;
	// patterns
	@Pattern(regexp="[0-9]{5}", message = "Només 5 números")
	private String codiPostal;
	// telefonFinlandia
	@telefonFinlandia
	private String telefon;
	// getters setters
	
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getCodiPostal() {
		return codiPostal;
	}
	public void setCodiPostal(String codiPostal) {
		this.codiPostal = codiPostal;
	}
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}
	public String getIdiomes() {
		return idiomes;
	}
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}
	public String getConneixements() {
		return conneixements;
	}
	public void setConneixements(String conneixements) {
		this.conneixements = conneixements;
	}
	public String getNom() {
		return nom;
	}
	public String getDepartament() {
		return departament;
	}
	public void setDepartament(String departament) {
		this.departament = departament;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
}
