package curs_de_05_AplicacionesWeb.validacionsPersonalitzades;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class validarTelefonDeFinlandia implements ConstraintValidator<telefonFinlandia, String> {
	private String prefixTelefonFinlandia;
	
	@Override
	public void initialize(telefonFinlandia annotationTelfFinlandia) {
		prefixTelefonFinlandia = annotationTelfFinlandia.value();
	}

	@Override
	public boolean isValid(String arg0, ConstraintValidatorContext arg1) {
		boolean resultat = false;
		
		if(arg0 != null) {
			if((arg0.startsWith(prefixTelefonFinlandia)) && (arg0.length() == 12)) {
				resultat = true;
			}
		} else {
			resultat = false;
		}
		return resultat;
	}
}
