package curs_de_05_AplicacionesWeb.validacionsPersonalitzades;


// ? 
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import javax.validation.Constraint;

import org.springframework.messaging.handler.annotation.Payload;

@Constraint(validatedBy = validarTelefonDeFinlandia.class)
@Target( {ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface telefonFinlandia {
	public String value() default "358";
	
	String message() default "Els telèfons de Finlandia tenen el prefix 358 + 9 xidfres.";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
