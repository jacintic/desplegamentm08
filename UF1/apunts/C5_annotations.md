# Java annotations.  
  
### @Qualifier  
Serveix per expecificar quina de les subclass s'ha d'instanciar.  
#### ! 
A un dels dos se li posa una ID com a `@Component("mantInferMedica")`  
```
@Autowired
	@Qualifier("mantInferMedica")
	private MantenimentInfermeriaInterface mantenimentMedicaments;
	

///////////////// a medicaments
@Component("mantInferMedica")
```
Si no el posem a un i si a l'altre peta  
### init() destroy() => @PostConstruct, @PreDestroy  
Nomes es poden fer en el patro SINGLETON  

