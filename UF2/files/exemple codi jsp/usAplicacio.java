package exercici_05_a_07;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;





public class usAplicacio {
	
	static public List<Tripulant> inicialitzarTripulants() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres:
		// https://stackoverflow.com/questions/55096855/how-to-ask-for-prototype-bean-in-spring-service-class-without-applicationcontext
		// Al final de todo, la respuesta del 11 de marzo del 2019 de Andy Brown.
		
		ServiceClassMultiparametres serviceClassMultiparametres = contexteAmbClasseConfig.getBean("serviceClassMultiparametres", ServiceClassMultiparametres.class);
		
		ArrayList<ObjIniProtoTypeBeanMultiparametres> llistaObjIniProtoTypeBeanMultiparametres = new ArrayList();
		ObjIniProtoTypeBeanMultiparametres tripulant_1 = new ObjIniProtoTypeBeanMultiparametres("10", "tripulant_10", LocalDate.now(), 100);
		ObjIniProtoTypeBeanMultiparametres tripulant_2 = new ObjIniProtoTypeBeanMultiparametres("20", "tripulant_20", LocalDate.now(), 200);
		ObjIniProtoTypeBeanMultiparametres tripulant_3 = new ObjIniProtoTypeBeanMultiparametres("30", "tripulant_30", LocalDate.now(), 100);
		llistaObjIniProtoTypeBeanMultiparametres.add(tripulant_1);
		llistaObjIniProtoTypeBeanMultiparametres.add(tripulant_2);
		llistaObjIniProtoTypeBeanMultiparametres.add(tripulant_3);
		
		serviceClassMultiparametres.demoMethod(llistaObjIniProtoTypeBeanMultiparametres);
		
		// Crearem 1 Organitzacio.
		// Crearem 3 departaments i els ficarem en "ArrayList<Departament> llistaDepartaments" de Organitzacio.
		// Crearem 3 tripulants per a cada departament i els anirem ficant en "ArrayList<Tripulant> llistaTripulants" de Departament.
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres.getTripulantMultiparametres();				
	}
	
	
	
	static public List<Departament> inicialitzarDepartaments_v2() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres (versió 2):
		
		ServiceClassMultiparametres2_v2 serviceClassMultiparametres2_v2 = contexteAmbClasseConfig.getBean("serviceClassMultiparametres2_v2", ServiceClassMultiparametres2_v2.class);
		
		List<Integer> llistaIdsDepartament = Arrays.asList(200, 201, 202, 203);
		List<String> llistaNomsDepartament = Arrays.asList("departament_200", "departament_201", "departament_202", "departament_203");
		List<String> llistaEmailsDepartament = Arrays.asList("departament_200@gmail.cat", "departament_201@gmail.cat", "departament_202@gmail.cat", "departament_203@gmail.cat");
		
		serviceClassMultiparametres2_v2.demoMethod_v2(llistaIdsDepartament, llistaNomsDepartament, llistaEmailsDepartament);
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres2_v2.getLlistaDepartamentsMultiparametres();
	}
	
}
