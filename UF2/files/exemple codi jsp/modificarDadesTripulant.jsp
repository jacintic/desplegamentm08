<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 <%@page import="exercici_05_a_07.Tripulant"%>
 <%@page import="exercici_05_a_07.Departament"%>
 <%@page import="exercici_05_a_07.TripulantMesDepartaments"%>
 <%@page import="java.util.List"%>
 <%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>CCCP Leonov</title>
	</head>
	
	<body>
		modificarDadesTripulant.jsp
		<br>
		<br>
		<br>
		${error}
		<br>
		<br>
		<form:form action="procesarDadesTripulant" modelAttribute="tripulantTrobat">
			DNI usuari: <form:input path="dni"/>  <form:errors path="dni" Style="color:red"></form:errors>
			<br>
			Nom usuari: <form:input path="nom"/>
			<br>
			Cognom: <form:input path="cognom"/> <form:errors path="cognom" Style="color:red"></form:errors>
			<br>
			Contrasenya: <form:input path="contrasenya"/> <form:errors path="contrasenya" Style="color:red"></form:errors>
			<br>
			Edat: <form:input path="edat"/> <form:errors path="edat" Style="color:red"></form:errors>
			<br>
			Data creació (m/d/aa): <form:input path="dataCreacio"/> <form:errors path="dataCreacio" Style="color:red"></form:errors>
			<br>
			Email: <form:input path="email"/> <form:errors path="email" Style="color:red"></form:errors>
			<br>
			<br>
			Departament: 
			
			<%
			List<Departament> llistaDepartaments = new ArrayList<Departament>();
			Tripulant tripulantTmp = new Tripulant();
			
			System.out.println();
			System.out.println("--------");
			
			tripulantTmp = (Tripulant) request.getAttribute("tripulantTrobat");
			System.out.println("tripulantTmp.getDepartament() = "  + tripulantTmp.getDepartament());

			llistaDepartaments = (List<Departament>) request.getAttribute("llistaDepartaments");
					
			System.out.println("llistaDepartaments.size() = " + llistaDepartaments.size());
			
			int j = 1;
			for(Departament protoTypeBeanTmp : llistaDepartaments) {
				System.out.println(j + ": DEPARTAMENT.getId() = " + protoTypeBeanTmp.getId() +
						"\n   tripulantTmp.getDepartament() = " + tripulantTmp.getDepartament() +
						"\n   DEPARTAMENT.getNom() = " + protoTypeBeanTmp.getNom());
				
				if (protoTypeBeanTmp.getId() == tripulantTmp.getDepartament() ){
				%>
					&nbsp;&nbsp;&nbsp;&nbsp;<form:radiobutton path="departament" value="<%=protoTypeBeanTmp.getId()%>" label="<%=protoTypeBeanTmp.getNom()%>" checked="checked"/>
				<%
				} else { 
				%>
					&nbsp;&nbsp;&nbsp;&nbsp;<form:radiobutton path="departament" value="<%=protoTypeBeanTmp.getId()%>" label="<%=protoTypeBeanTmp.getNom()%>"/>
				<%
				}
				
				j++;
			}
			%>

			<br>
			<br>
			<input type="submit">
		</form:form>
		
		
	</body>
</html>
