package exercici_05_a_07;

import java.time.LocalDate;

public class ObjIniProtoTypeBeanMultiparametres {
	private String dni;
	private String nom;
	private LocalDate dataCreacio;
	private int departament;
	
	
	public ObjIniProtoTypeBeanMultiparametres(String dni, String nom, LocalDate dataCreacio, int departament) {
		this.dni = dni;
		this.nom = nom;
		this.dataCreacio = dataCreacio;
		this.departament = departament;
	}

	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	public int getDepartament() {
		return departament;
	}

	public void setDepartament(int departament) {
		this.departament = departament;
	}
	
	
	
}
