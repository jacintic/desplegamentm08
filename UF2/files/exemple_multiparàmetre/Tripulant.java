package exercici_05_a_07;

import java.time.LocalDate;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import exercici_05_a_07.validacionsPersonalitzades.validacioDni;


@Component //(SpringConstants.COMPONENT_WITH_PARAMS)				// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
@Scope("prototype")
public class Tripulant {
	@validacioDni
	private String dni;
	
	private String nom;
	
	@NotNull(message = "El cognom ha de tenir com a mínim 7 caràcters i com a màxim 100.")
	@Size(min = 7, max = 100, message = "El cognom ha de tenir com a mínim 7 caràcters i com a màxim 100.")
	private String cognom;
	
	private String login;
	
	@NotNull(message = "La contrasenya ha de tenir com a mínim 7 caràcters.")
	@NotEmpty(message = "La contrasenya ha de tenir com a mínim 7 caràcters.")
	@NotBlank(message = "La contrasenya ha de tenir com a mínim 7 caràcters.")
	@Size(min = 7, message = "La contrasenya ha de tenir com a mínim 7 caràcters.")
	private String contrasenya;
	
	private int departament;
	private String conneixements;
	private String ciutatNaixement;
	private String idiomes;
	
	@Min(value = 18, message = "Ha de tenir com a mínim 18 anys.")
	private int edat;
	
	@NotNull(message = "Format d'email incorrecte.")
	@Pattern(regexp="[a-zA-Z0-9][a-zA-Z0-9]{0,}[@][a-zA-Z0-9][a-zA-Z0-9]{0,}[.][a-zA-Z0-9][a-zA-Z0-9]{0,}", message="Format d'email incorrecte.")
	private String email;
	
	private String telefon;
	
	@FutureOrPresent(message = "Ha de tenir una data de creació igual al dia d'avui o futurible.")
	private LocalDate dataCreacio;
	
	
	public Tripulant(ObjIniProtoTypeBeanMultiparametres objIniProtoTypeBeanMultiparametres) {	
		this.dni = objIniProtoTypeBeanMultiparametres.getDni();
		this.nom = objIniProtoTypeBeanMultiparametres.getNom();
		this.dataCreacio = objIniProtoTypeBeanMultiparametres.getDataCreacio();
		this.departament = objIniProtoTypeBeanMultiparametres.getDepartament();
	}
	
	
	public Tripulant() {
	}
	
	
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		return true;
	}


	public Tripulant(String dni, String nom, LocalDate dataCreacio, int departament) {
		this.dni = dni;
		this.nom = nom;
		this.dataCreacio = dataCreacio;
		this.departament = departament;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getCognom() {
		return cognom;
	}


	public void setCognom(String cognom) {
		this.cognom = cognom;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getContrasenya() {
		return contrasenya;
	}


	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}


	public int getDepartament() {
		return departament;
	}


	public void setDepartament(int departament) {
		this.departament = departament;
	}


	public String getConneixements() {
		return conneixements;
	}


	public void setConneixements(String conneixements) {
		this.conneixements = conneixements;
	}


	public String getCiutatNaixement() {
		return ciutatNaixement;
	}


	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}


	public String getIdiomes() {
		return idiomes;
	}


	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}


	public int getEdat() {
		return edat;
	}


	public void setEdat(int edat) {
		this.edat = edat;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefon() {
		return telefon;
	}


	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}


	public LocalDate getDataCreacio() {
		return dataCreacio;
	}


	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	
	
	
	
}
