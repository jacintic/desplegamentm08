-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 22, 2021 at 04:20 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `LeonovBD_05_a_08`
--

-- --------------------------------------------------------

--
-- Table structure for table `departament`
--

CREATE TABLE `departament` (
  `id` int(3) NOT NULL,
  `nom` varchar(128) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nauId` int(3) DEFAULT NULL,
  `capDeDepartament` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `departament`
--

INSERT INTO `departament` (`id`, `nom`, `email`, `nauId`, `capDeDepartament`) VALUES
(2, 'Navegació', 'navegacio@rotarran.ik', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nau`
--

CREATE TABLE `nau` (
  `id` int(3) NOT NULL,
  `nom` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nau`
--

INSERT INTO `nau` (`id`, `nom`) VALUES
(1, 'Rotarran');

-- --------------------------------------------------------

--
-- Table structure for table `tripulant`
--

CREATE TABLE `tripulant` (
  `id` int(3) NOT NULL,
  `nom` varchar(128) DEFAULT NULL,
  `cognom` varchar(128) DEFAULT NULL,
  `login` varchar(128) DEFAULT NULL,
  `departamentId` int(3) DEFAULT NULL,
  `coneixements` text DEFAULT NULL,
  `ciutatNaixement` varchar(128) DEFAULT NULL,
  `idiomes` text DEFAULT NULL,
  `edat` int(3) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telefon` varchar(12) DEFAULT NULL,
  `dataCreacio` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tripulant`
--

INSERT INTO `tripulant` (`id`, `nom`, `cognom`, `login`, `departamentId`, `coneixements`, `ciutatNaixement`, `idiomes`, `edat`, `email`, `telefon`, `dataCreacio`) VALUES
(1, 'Worf', 'Pagh', 'worfPagh', 1, 'astronomia   i   navegació', 'Cronos', 'klingon', 35, 'worf@ratorran.ik', '948751258915', '2021-01-23'),
(2, 'Kurn', 'Pagh', 'KurnPagh', 1, 'Astronomia', 'Cronos', 'klingon', 58, 'kurnpagh@imperiklingon.ik', '984587562158', '2021-01-21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departament`
--
ALTER TABLE `departament`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_NAU_ID` (`nauId`),
  ADD KEY `FK_CAP_DE_DEPARTAMENT` (`capDeDepartament`);

--
-- Indexes for table `nau`
--
ALTER TABLE `nau`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tripulant`
--
ALTER TABLE `tripulant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_DEPARTAMENT_ID` (`departamentId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departament`
--
ALTER TABLE `departament`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nau`
--
ALTER TABLE `nau`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tripulant`
--
ALTER TABLE `tripulant`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `departament`
--
ALTER TABLE `departament`
  ADD CONSTRAINT `FK_CAP_DE_DEPARTAMENT` FOREIGN KEY (`capDeDepartament`) REFERENCES `tripulant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_NAU_ID` FOREIGN KEY (`nauId`) REFERENCES `nau` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
