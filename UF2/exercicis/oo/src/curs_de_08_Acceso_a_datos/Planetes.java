package curs_de_08_Acceso_a_datos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="planetes")
public class Planetes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="descripcio")
	private String descripcio;
	
	@Column(name="dataDescubriment")
	private LocalDate dataDescubriment;
	
	// CONSTRUCTORS
	
	public Planetes() {
	}
	
	
	public Planetes(String nom, String descripcio, LocalDate dataDescubriment) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.dataDescubriment = dataDescubriment;
	}

	// GETTERS/SETTERS

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getDescripcio() {
		return descripcio;
	}


	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}


	public LocalDate getDataDescubriment() {
		return dataDescubriment;
	}


	public void setDataDescubriment(LocalDate dataDescubriment) {
		this.dataDescubriment = dataDescubriment;
	}

	
	// TOSTRING

	@Override
	public String toString() {
		return "Planetes [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", dataDescubriment="
				+ dataDescubriment + "]";
	}
	
	
	
}
