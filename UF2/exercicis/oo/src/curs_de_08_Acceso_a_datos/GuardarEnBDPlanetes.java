package curs_de_08_Acceso_a_datos;

import java.time.LocalDate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GuardarEnBDPlanetes {

	public static void main(String[] args) {
		System.out.println("Creem l'objecte SessionFactory.");
		// session factory / session (hibernate)
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		
		System.out.println("Creem l'objecte Session.");
		Session elMeuSession = elMeuFactory.openSession();
		
		System.out.println("Creem 2 objectes de tipus Planetes.");
		// planet object
		Planetes planeta_1 = new Planetes("Plutó", "Planeta blau", LocalDate.of(1610,12,31));
		Planetes planeta_2 = new Planetes("Saturn", "Planeta gaseós", LocalDate.of(1610,1,18));
		
		
		// session
		try {
			System.out.println("Iniciem la transacció.");
			
			elMeuSession.beginTransaction();
						
			elMeuSession.save(planeta_1);
			
			// flush i clear per continuar amb l'insert
			
			elMeuSession.flush();
			
			elMeuSession.clear();
			
			elMeuSession.save(planeta_2);
			
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Acabem la transacció de Mart. Registre INSERTAT en la BD.");
			System.out.println("Acabem la transacció de Saturn. Registre INSERTAT en la BD.");
			
			// llegir id d'un planeta
			// s'ha de reiniciar la sessio
			
			elMeuSession.beginTransaction();
			
			System.out.println("Lecutra del recistre amb id = " + planeta_1.getId());
			
			Planetes planetaLlegitDeLaBD = elMeuSession.get(Planetes.class, planeta_1.getId());
			
			System.out.println("Registre llegit de la BD: " + planetaLlegitDeLaBD);
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Hem acabat de llegir un registre de la BD.");
			
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
		
	}

}
