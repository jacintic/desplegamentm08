package exercici_05_a_08.controladors;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import exercici_05_a_08.DAOs.*;
import exercici_05_a_08.entitats.Tripulant;

@Controller
@RequestMapping("/tripulant")
public class Controlador {
	
	@Autowired
	private TripulantDAOInterface tripulantDAO;
	
	@RequestMapping("/llistaTripulants")
	public String llistarTripulants(Model elModel) {
		
		List<Tripulant> llistaTripulantsDelDAO = tripulantDAO.getTripulants();
		elModel.addAttribute("llistaTripulants", llistaTripulantsDelDAO);
		
		return "llista_tripulants";
	}
	
	@RequestMapping("/formulariInsertTripulant")
	public String mostrarFormulariInsertTripulant(Model elModel) {
		
		Tripulant nouTripulant = new Tripulant();
		
		elModel.addAttribute("elNouTripulant", nouTripulant);
		
		return "formulari_insertar_tripulant";
	}
	
	//procesarAltaTripulant
	@PostMapping("/procesarAltaTripulant")
	public String donarDAltaTripulant(@ModelAttribute("elNouTripulant") Tripulant tripulantNou) {
		// conectamos el objecto del formulario (elNouTripulant) 
		// con el de la validación de formulario
		tripulantNou.setDataCreacio(LocalDateTime.now());
		tripulantDAO.insertarTripulant(tripulantNou);
		return "redirect:/tripulant/llistaTripulants";
	}
	
	// /tripulant/formulariUpdateTripulant 
	// updatejar tripulant
	@GetMapping("formulariUpdateTripulant")
	public String mostrarFormulariUpdateTripulant(@RequestParam("tripulantId") int tripulantId,
													Model elModel) {
		// get tripulant id
		// s'ha dafegir getTirpulant
		Tripulant tripulantTmp = tripulantDAO.getTripulant(tripulantId);
		
		
		elModel.addAttribute("tripulantPerUpdatejar", tripulantTmp);
		
		return "formulari_updatejar_tripulant";
	}
	
	//procesarupdate
	@PostMapping("/procesarUpdateTripulant")
	public String updatejarTripulant(@ModelAttribute("tripulantPerUpdatejar") Tripulant tripulantPerUpdatejar) {
		
		tripulantDAO.updatejarTripulant(tripulantPerUpdatejar);
		
		//despres de fer l'update ens retorna una llista dels tripulants a la BD		
		return "redirect:/tripulant/llistaTripulants";
	}
	
	

	
}	
