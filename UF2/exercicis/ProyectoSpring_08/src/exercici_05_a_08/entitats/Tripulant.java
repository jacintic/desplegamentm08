package exercici_05_a_08.entitats;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tripulant")
public class Tripulant {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="cognom")
	private String cognom;
	
	@Column(name="email")
	private String email;
	
	@Column(name="dataCreacio")
	private LocalDateTime dataCreacio;
	
	@Column(name="departamentId")
	private int departamentId;
	
	public Tripulant() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	public int getDepartamentId() {
		return departamentId;
	}

	public void setDepartamentId(int departamentId) {
		this.departamentId = departamentId;
	}
	
	@Override
	public String toString() {
		return "Tripulant [id=" + this.id + ", nom=" + this.nom + ", cognom=" + this.cognom + 
				", email=" + this.email + ", dataCreacio=" + this.dataCreacio + ", departamentId=" + this.departamentId + "]";
	}
}
