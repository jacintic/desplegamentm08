package exercici_05_a_08.DAOs;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import exercici_05_a_08.entitats.Tripulant;

@Repository
public class TripulantDAOClasse implements TripulantDAOInterface{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	@Override
	public List<Tripulant> getTripulants() {
		
		Session laMevaSessio = sessionFactory.getCurrentSession();
		Query<Tripulant> queryLlistaTripulants = laMevaSessio.createQuery("from Tripulant", Tripulant.class);
		
		List<Tripulant> llistaTripulantDeLaBD = queryLlistaTripulants.getResultList();
		
		return llistaTripulantDeLaBD;
	}

	@Override
	@Transactional
	public void insertarTripulant(Tripulant tripulantNou) {
		//Obtenir la session: 
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		// Gravem el tripulant en la BD:
		laMevaSessio.save(tripulantNou);
		
	}

	@Override
	@Transactional
	public Tripulant getTripulant(int tripulantId) {
		// Obtenir la session:
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		// omplir obtenri el tripulant per id de la BD
		Tripulant tripulantTmp = laMevaSessio.get(Tripulant.class,  tripulantId);
		return tripulantTmp;
	}

	@Override
	@Transactional
	public void updatejarTripulant(Tripulant tripulantPerUpdatejar) {
		
		// Obtenir la session:
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		// Updatejem el tripulant en la BD:
		laMevaSessio.update(tripulantPerUpdatejar);
		
	}


	
}
