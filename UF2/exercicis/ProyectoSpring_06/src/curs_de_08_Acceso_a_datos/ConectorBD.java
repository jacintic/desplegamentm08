package curs_de_08_Acceso_a_datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConectorBD {

	public static void main(String[] args) {
		

			
				// Connexió a BBDD
			String servidorBDUrl = "jdbc:mysql://localhost:3306/LeonovBD?useSSL=false";
			String usuari = "root";
			String contrasenya = "admin";
			
			System.out.println("INICIANT CONEXIO AMB LA BD: " + servidorBDUrl);
			
			
			try {
				Connection conexio = DriverManager.getConnection(servidorBDUrl, usuari, contrasenya);
				
				System.out.println("CONEXIO AMB EXIT");
			} catch (SQLException e) {
				System.err.println("e.getMessage() = " + e.getMessage());
				e.printStackTrace();
			}



	}

}
