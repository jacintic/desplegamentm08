package curs_de_05_AplicacionesWeb;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Departament {

	private int ID;
	private String nom;
	private String email;
	private Organitzacio organitzacio;
	private Personal capDeDepartament;
	private ArrayList<Personal> llistaPersonal;
	
	// constructor
	public Departament(int iD, String nom, String email, Organitzacio organitzacio, Personal capDeDepartament,
			ArrayList<Personal> llistaPersonal) {
		super();
		ID = iD;
		this.nom = nom;
		this.email = email;
		this.organitzacio = organitzacio;
		this.capDeDepartament = capDeDepartament;
		this.llistaPersonal = llistaPersonal;
	}
	
	// constructor v3 maybe?
	public Departament(int iD, String nom, String email) {
		super();
		ID = iD;
		this.nom = nom;
		this.email = email;
	}
	
	// getters and setters
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}
	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}
	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}
	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}
	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}
	

	//tostring
	@Override
	public String toString() {
		return "Departament [ID=" + ID + ", nom=" + nom + ", email=" + email + ", organitzacio=" + organitzacio
				+ ", capDeDepartament=" + capDeDepartament + ", llistaPersonal=" + llistaPersonal + "]";
	}
	
}
