package curs_de_05_AplicacionesWeb;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component		// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
public class ServiceClassMultiparametres2_v3 {
	private final BeanFactory factory;
	private List<Departament> llistaDepartamentsMultiparametres;

	
	@Autowired
	public ServiceClassMultiparametres2_v3(final BeanFactory f) {
		this.factory = f;
	}

	
	public void demoMethod_v3(List<Integer> llistaPosicionsEnArray, List<Integer> llistaIdsDepartament, List<String> llistaNomsDepartament, List<String> llistaEmailsDepartament) {
		// Various versions of getBean() method return an instance of the specified bean.
		
		// Recorre tota la llista 'llistaIdsDepartament' a saco creant un flux amd les dades que conté (stream()) i 
		// per a cada element (param) aplica la funció que li passem dins del map() de manera que ens retorna
		// un flux amb tots els resultats (un flux amb els objectes de tipus Departament creats amb
		// 'factory.getBean(Departament.class, param)' i inicialitzats segons el
		// contructor Departament.public Departament(int id){}).
		//
		// Converteix el flux resultant en una llista gràcies al collect(Collectors.toList()). 
		// El collect() és el que canviarà el flux de dades en un altre cosa que serà una llista gràcies al paràmetre 'Collectors.toList()'.

		
		/*
		// Això sí que funciona:
		this.llistaDepartamentsMultiparametres = llistaIdsDepartament.stream().map(param -> factory.getBean(Departament.class, param, llistaNomsDepartament.get(0), llistaEmailsDepartament.get(0)))
				.collect(Collectors.toList());
		*/
		
		// En 'param' hi ha les posicions ocupades de les altres 3 llistes (llistaIdsDepartament, llistaNomsDepartament i llistaEmailsDepartament)
		// ja que és lo que transporta la llista 'llistaPosicionsEnArray'. 
		this.llistaDepartamentsMultiparametres = llistaPosicionsEnArray.stream().map(param -> factory.getBean(Departament.class, llistaIdsDepartament.get(param), llistaNomsDepartament.get(param), llistaEmailsDepartament.get(param)))
				.collect(Collectors.toList());
		
		
		/*
		for (int i = 0; i < llistaIdsDepartament.size(); i++) {
		
			// 1a forma:
			// FALLA el add().
			llistaDepartamentsMultiparametres.add(factory.getBean(Departament.class, llistaIdsDepartament.get(i), llistaNomsDepartament.get(i), llistaEmailsDepartament.get(i)));
			
			
			// 2a forma:
			Departament tiger = factory.getBean(Departament.class, llistaIdsDepartament.get(i), llistaNomsDepartament.get(i), llistaEmailsDepartament.get(i));
			System.out.println("----------------");
			System.out.println(tiger.getId() + ", " + tiger.getEmail());
			System.out.println("----------------");
			this.llistaDepartamentsMultiparametres.add(tiger);		// FALLA el add().
			
			
			// 3a forma:
			AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
			Departament tiger2 = contexteAmbClasseConfig.getBean(Departament.class, llistaIdsDepartament.get(i), llistaNomsDepartament.get(i), llistaEmailsDepartament.get(i));
			System.out.println("----------------");
			System.out.println(tiger2.getId() + ", " + tiger2.getEmail());
			System.out.println("----------------");
			this.llistaDepartamentsMultiparametres.add(tiger2);		// FALLA el add().
		}
		*/
	}
	

	public List<Departament> getLlistaDepartamentsMultiparametres() {
		return llistaDepartamentsMultiparametres;
	}


	
}
