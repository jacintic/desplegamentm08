package curs_de_05_AplicacionesWeb;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class usAplicacio {
	
	static public List<Personal> inicialitzarPersonal() {
		System.out.println("0");
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		System.out.println("1");
		ServiceClassMultiparametres serviceClassMultiparametres = contexteAmbClasseConfig.getBean("serviceClassMultiparametres", ServiceClassMultiparametres.class);
		System.out.println("2");
		// Personal
		List<Integer> llistaPosicionsEnArray = Arrays.asList(0,1,2);
		List<String> llistaDnisPersonal = Arrays.asList("1", "15730482F", "17394729H");
		List<String> llistaNomsPersonal = Arrays.asList("Manuel", "Böörk", "Strinfer");
		List<LocalDate> llistaDataCreacioPersonal = Arrays.asList(LocalDate.now(), LocalDate.now(), LocalDate.now());
		List<Integer> llistaDept = Arrays.asList(1,2,3);
		List<String> llistaPass = Arrays.asList("a","def","ghi");
		System.out.println("3");
		// build Personal List
		serviceClassMultiparametres.demoMethodPersonal(llistaPosicionsEnArray, 
														llistaDnisPersonal, 
														llistaNomsPersonal, 
														llistaDataCreacioPersonal, 
														llistaDept, 
														llistaPass);
		System.out.println("4");
		contexteAmbClasseConfig.close();
		
		System.out.println("5");
		return serviceClassMultiparametres.getLlistaPersonalMultiparametres();
	}
	
	static public List<Departament> inicialitzarDepartaments_v3() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres (versió 3):
		
		ServiceClassMultiparametres2_v3 serviceClassMultiparametres2_v3 = contexteAmbClasseConfig.getBean("serviceClassMultiparametres2_v3", ServiceClassMultiparametres2_v3.class);
		
		// La llista 'llistaPosicionsEnArray' conté les posicions que es fan servir en les llistes, hi haurà 4 departaments i --> 0 fins a 3.		
		List<Integer> llistaPosicionsEnArray = Arrays.asList(0, 1, 2, 3);
		List<Integer> llistaIdsDepartament = Arrays.asList(1, 2, 3, 4);
		List<String> llistaNomsDepartament = Arrays.asList("departament_301", "departament_302", "departament_303", "departament_304");
		List<String> llistaEmailsDepartament = Arrays.asList("departament_301@gmail.cat", "departament_302@gmail.cat", "departament_303@gmail.cat", "departament_304@gmail.cat");
		
		serviceClassMultiparametres2_v3.demoMethod_v3(llistaPosicionsEnArray, llistaIdsDepartament, llistaNomsDepartament, llistaEmailsDepartament);
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres2_v3.getLlistaDepartamentsMultiparametres();
	}
}

