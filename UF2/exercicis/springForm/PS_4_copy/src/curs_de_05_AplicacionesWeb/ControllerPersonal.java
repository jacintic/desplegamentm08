package curs_de_05_AplicacionesWeb;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
//@RequestMapping("/personal")
public class ControllerPersonal {

	// declarem parametres de la BD
	List<Personal> llistaProtoTypeBeanMultiparametresTripulants;
	List<Departament> llistaProtoTypeBeanMultiparametresDepartaments_v3;

	@RequestMapping("/") // Aquesta javaAnnotation determina quina URL ha de fer servir l'usuari per a
							// demanar la vista "vistaExemple_1.jsp".
	public String mostrarVista1() { // Com que no posa res, equival a @RequestMapping("/") que vol dir que serà
									// l'arrel del nostre
									// projecte, és a dir, que serà la vista que es carregarà per defecte si no
									// s'especifica cap altre
		// inicialitzem parametres de Personal
		llistaProtoTypeBeanMultiparametresTripulants = usAplicacio.inicialitzarPersonal(); // en el paràmetre de
																							// @RequestMapping.
		// return "vistaExemple_1"; // Amb aquesta java Annotation estem determinant que
		// el mètode mostrarVista1() ha de rastrejar pel nostre

		// departaments
		llistaProtoTypeBeanMultiparametresDepartaments_v3 = usAplicacio.inicialitzarDepartaments_v3();

		return "inici";
	}

	@RequestMapping("/login")
	public String mostrarFormulari(Model model) {
		/*
		 * // inicialitzem parametres de Personal
		 * llistaProtoTypeBeanMultiparametresTripulants =
		 * usAplicacio.inicialitzarPersonal();
		 */
		// print personal
		System.out.println();
		System.out.println("------llistaProtoTypeBeanMultiparametresTripulants------");

		int j = 1;
		for (Personal personalTmp : llistaProtoTypeBeanMultiparametresTripulants) {
			System.out.println(j + ": TRIPULANT = " + personalTmp + "\n      tripulantTmp.getDni() = "
					+ personalTmp.getDni() + "\n      tripulantTmp.getNom() = " + personalTmp.getNom()
					+ "\n      tripulantTmp.getDepartament() = " + personalTmp.getDepartament()
					+ "\n      tripulantTmp.getDataCreacio() = " + personalTmp.getDataCreacio()
					+ "\n      tripulantTmp.getPass() = " + personalTmp.getContrasenya());
			j++;
		}

		/*
		 * Personal empleat = new Personal(); empleat.setNom("Pepito");
		 * System.out.println(empleat.getNom()); model.addAttribute("el_empleat",
		 * empleat);
		 */
		String message = "";
		model.addAttribute("dniErr", message);
		return "login";

	}

	// resposta login
	@RequestMapping("/loginRes")
	public String validarFormulari(HttpServletRequest request, Model model) {
		// get dni
		String dni = request.getParameter("dni");
		// initialize message
		String message = "ERROR: Wrong DNI";
		// check dni
		for (Personal p : llistaProtoTypeBeanMultiparametresTripulants) {
			if (p.getDni().equals(dni)) {
				// get pass
				String pass = request.getParameter("pass");
				if (p.getContrasenya().equals(pass)) {
					message = "login successful";
					model.addAttribute("loginSuccess", message);
					return "loginRes";
				}
				message = "ERROR: Wrong Pass";
				break;
			}
		}
		model.addAttribute("dniErr", message);
		return "login";
	}

	// resposta login
	@RequestMapping("/signup")
	public String mostrarSignup(HttpServletRequest request, Model model) {
		// get dni
		String dni = request.getParameter("dni");
		// get nom
		String nom = request.getParameter("nom");
		// get dni
		String pass = request.getParameter("pass");
		// string message
		String message = "";
		// boolean to interrupt dni lookup
		boolean dniDup = false;
		// check for params within the DB
		if (dni != null) {
			for (Personal p : llistaProtoTypeBeanMultiparametresTripulants) {
				if (p.getDni() != null && p.getDni().equals(dni)) {
					message = "ERROR: DNI already exists";
					dniDup = true;
					model.addAttribute("dniErr", message);
					return "signup";
				}
			}
		}
		if (!dniDup && nom != null && pass != null) {
			message = "Signup successful";
			Personal p = new Personal(dni, nom, LocalDate.now(), 1, pass);
			llistaProtoTypeBeanMultiparametresTripulants.add(p);
			System.out.println("Nou Personal creat: \n" + p);
			System.out.println("\n-----Llistat Personal:");
			// llistant tot el personal
			int j = 1;
			for (Personal personalTmp : llistaProtoTypeBeanMultiparametresTripulants) {
				System.out.println(j + ": TRIPULANT = " + personalTmp + "\n      tripulantTmp.getDni() = "
						+ personalTmp.getDni() + "\n      tripulantTmp.getNom() = " + personalTmp.getNom()
						+ "\n      tripulantTmp.getDepartament() = " + personalTmp.getDepartament()
						+ "\n      tripulantTmp.getDataCreacio() = " + personalTmp.getDataCreacio()
						+ "\n      tripulantTmp.getPass() = " + personalTmp.getContrasenya());
				j++;
			}
		}
		model.addAttribute("dniErr", message);
		return "signup";
	}

	// mostrar modificar dades usuari
	@RequestMapping("/modUser")
	public String modUser(Model model) {
		// get personal by ID (hardcoded)
		Personal myPerson = null;
		for (Personal personalTmp : llistaProtoTypeBeanMultiparametresTripulants) {
			if (personalTmp.getDni().equals("1")) {
				myPerson = personalTmp;
			}
		}
		// add personal to model
		model.addAttribute("personal", myPerson);
		// add list of departments to model
		model.addAttribute("departaments", llistaProtoTypeBeanMultiparametresDepartaments_v3);
		// return view
		return "modificarDadesTripulant";
	}

	// mostrar modificar dades usuari
	@RequestMapping("/showUser")
	public String showUser(HttpServletRequest request, Model model) {
		/*
		 * ATENTION this method assumes the DNI attribute didn't change, if it changes
		 * this method will malfunction
		 * */
		// get data from form to make update
		String dni = request.getParameter("dni");
		String nom = request.getParameter("nom");
		String contrasenya = request.getParameter("contrasenya");
		String dept = request.getParameter("departament");
		// get personal by ID (hardcoded)
		Personal myPerson = null;
		for (Personal personalTmp : llistaProtoTypeBeanMultiparametresTripulants) {
			if (personalTmp.getDni().equals(dni)) {
				personalTmp.setNom(nom);
				personalTmp.setContrasenya(contrasenya);
				personalTmp.setDepartament(Integer.parseInt(dept));
				myPerson = personalTmp;
			}
		}
		// add personal to model
		model.addAttribute("personal", myPerson);
		return "mostrarDadesUsuari";
	}
}
