package curs_de_05_AplicacionesWeb;

import java.time.LocalDate;

public class ObjIniProtoTypeBeanMultiparametres {
	
		private String dni;
		private String nom;
		private LocalDate dataCreacio;
		private int departament;
		private String password;
		
		
		public ObjIniProtoTypeBeanMultiparametres(String dni, String nom, LocalDate dataCreacio, int departament) {
			this.dni = dni;
			this.nom = nom;
			this.dataCreacio = dataCreacio;
			this.departament = departament;
		}
		public ObjIniProtoTypeBeanMultiparametres(String dni, String nom, LocalDate dataCreacio, int departament, String password) {
			this.dni = dni;
			this.nom = nom;
			this.dataCreacio = dataCreacio;
			this.departament = departament;
			this.password = password;
		}

		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getDni() {
			return dni;
		}
		
		public void setDni(String dni) {
			this.dni = dni;
		}
		
		public String getNom() {
			return nom;
		}
		
		public void setNom(String nom) {
			this.nom = nom;
		}
		
		public LocalDate getDataCreacio() {
			return dataCreacio;
		}
		
		public void setDataCreacio(LocalDate dataCreacio) {
			this.dataCreacio = dataCreacio;
		}

		public int getDepartament() {
			return departament;
		}

		public void setDepartament(int departament) {
			this.departament = departament;
		}
		
		@Override
		public String toString() {
			return "ObjIniProtoTypeBeanMultiparametres [dni=" + dni + ", nom=" + nom + ", dataCreacio=" + dataCreacio
					+ ", departament=" + departament + ", password=" + password + "]";
		}
}
