package curs_de_05_AplicacionesWeb;

import java.util.ArrayList;

public class Organitzacio {
	
	private String nom;
	private ArrayList<Departament> llistaDepartaments;
	
	// constructor
	public Organitzacio(String nom, ArrayList<Departament> llistaDepartaments) {
		super();
		this.nom = nom;
		this.llistaDepartaments = llistaDepartaments;
	}
	
	// getters setters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Departament> getLlistaDepartaments() {
		return llistaDepartaments;
	}

	public void setLlistaDepartaments(ArrayList<Departament> llistaDepartaments) {
		this.llistaDepartaments = llistaDepartaments;
	}
	
	// tostring
	@Override
	public String toString() {
		return "Organitzacio [nom=" + nom + ", llistaDepartaments=" + llistaDepartaments + "]";
	}
	
}
