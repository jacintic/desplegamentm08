package curs_de_05_AplicacionesWeb;

import java.time.LocalDate;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Personal {
	private String dni;
	private String nom;
	private String cognom;
	private String email;
	private String login;
	private String contrasenya;
	private int departament;			// Aquí va el ID del departament al qual està assignat.String conneixementsString ciutatNaixement
	private String idiomes;
	private int edat;
	private String telefon;
	private LocalDate dataCreacio;
	
	//constructor empty
	public Personal() {
	}
	
	//constructor
	public Personal(String dni, String nom, String cognom, String email, String login, String contrasenya,
			int departament, String idiomes, int edat, String telefon, LocalDate dataCreacio) {
		super();
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.email = email;
		this.login = login;
		this.contrasenya = contrasenya;
		this.departament = departament;
		this.idiomes = idiomes;
		this.edat = edat;
		this.telefon = telefon;
		this.dataCreacio = dataCreacio;
	}
	
	public Personal(String dni, String nom, LocalDate dataCreacio, int departament, String contrasenya) {
		super();
		this.dni = dni;
		this.nom = nom;
		this.contrasenya = contrasenya;
		this.departament = departament;
		this.dataCreacio = dataCreacio;
	}

	// getters setters
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getContrasenya() {
		return contrasenya;
	}
	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}
	public int getDepartament() {
		return departament;
	}
	public void setDepartament(int departament) {
		this.departament = departament;
	}
	public String getIdiomes() {
		return idiomes;
	}
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	
	// tostring
	@Override
	public String toString() {
		return "Personal [dni=" + dni + ", nom=" + nom + ", cognom=" + cognom + ", email=" + email + ", login=" + login
				+ ", contrasenya=" + contrasenya + ", departament=" + departament + ", idiomes=" + idiomes + ", edat="
				+ edat + ", telefon=" + telefon + ", dataCreacio=" + dataCreacio + "]";
	}
	
}
