<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="curs_de_05_AplicacionesWeb.Personal"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Informació personal</h1>
	<%
			Personal personalTmp = new Personal();
			
			personalTmp = (Personal) request.getAttribute("personal");
	%>
	<p>DNI: <%=personalTmp.getDni()%></p>
	<p>Nom: <%=personalTmp.getNom()%></p>
	<p>Contrasenya: <%=personalTmp.getContrasenya()%></p>
	<p>Departament: <%=personalTmp.getDepartament()%></p>
</body>
</html>