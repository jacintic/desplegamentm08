<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page import="curs_de_05_AplicacionesWeb.Personal"%>
<%@page import="curs_de_05_AplicacionesWeb.Departament"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modificar Dades D'usuari</title>
</head>
<body>

<form:form action="showUser" modelAttribute="personal">
	DNI usuari: <form:input path="dni"/>
	<br>
	Nom usuari: <form:input path="nom"/>
	<br>
	Contrasenya: <form:input path="contrasenya"/> 
	<br>
	<%
			List<Departament> llistaDepartaments = new ArrayList<Departament>();
			Personal tripulantTmp = new Personal();
			
			tripulantTmp = (Personal) request.getAttribute("personal");
			llistaDepartaments = (List<Departament>) request.getAttribute("departaments");
			
			int j = 1;
			for(Departament protoTypeBeanTmp : llistaDepartaments) {
				System.out.println(j + ": DEPARTAMENT.getId() = " + protoTypeBeanTmp.getID() +
						"\n   DEPARTAMENT.getNom() = " + protoTypeBeanTmp.getNom());
				
				if (protoTypeBeanTmp.getID() == tripulantTmp.getDepartament() ){
				%>
					&nbsp;&nbsp;&nbsp;&nbsp;<form:radiobutton path="departament" value="<%=protoTypeBeanTmp.getID()%>" label="<%=protoTypeBeanTmp.getNom()%>" checked="checked"/>
				<%
				} else { 
				%>
					&nbsp;&nbsp;&nbsp;&nbsp;<form:radiobutton path="departament" value="<%=protoTypeBeanTmp.getID()%>" label="<%=protoTypeBeanTmp.getNom()%>"/>
				<%
				}
				
				j++;
			}
			%>
	<br>
	<input type="submit">
</form:form>

</body>
</html>