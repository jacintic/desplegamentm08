<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulari d'alta d'un tripulant - CCCP Leonov</title>
</head>
<body>
	<form:form action="procesarAltaTripulant" modelAttribute="el_doctor">
	Nom del tripulatn: <form:input path="nom"/>
	<!-- VALIDATION HIBERNATE -->
	<form:errors path="nom" style="color:red"></form:errors>
	
	<br/>
	<br/>
	Cognom del tripulant: <form:input path="cognom"/>
	<!-- Llistas desplegables -->
	 
	Departament (només 1): <form:select path="departament">
		<form:option value="infermeria">Infermeria</form:option>
		<form:option value="maquines">Màquines</form:option>
		<form:option value="pont" label="Pont"/>
	</form:select>
	 
	<!-- llista multi-opcio (pitjar control) -->
	<br>
	<br>
	Conneixements (sel·lecció múltiple): <form:select path="conneixements" multiple="true">
		<form:option value="biologia" label="Biologia"/>
		<form:option value="quimica" label="Quimica"/>
		<form:option value="antropologia" label="Antropologia"/>
		<form:option value="biologiaMolecular" label="Biologia molcular"/>
	</form:select>
	<!-- checkboxes and radiobuttons -->
	<br>
	<br>
	Seleccionar una ciutat d'origen: 
		<form:radiobutton path="ciutatNaixement" value="Barcelona" label="Barcelona"/>
		<form:radiobutton path="ciutatNaixement" value="Estocolm" label="Estocolm"/>
		<form:radiobutton path="ciutatNaixement" value="Helsinki" label="Helsinki"/>
		<form:radiobutton path="ciutatNaixement" value="Oslo" label="Oslo"/>
	<br>
	<br>
	Seleccionar els idiomes que sap: 
		<form:checkbox path="idiomes" value="castella" label="Castella"/>
		<form:checkbox path="idiomes" value="catala" label="Catala"/>
		<form:checkbox path="idiomes" value="finlandes" label="Finlandes"/>
		<form:checkbox path="idiomes" value="suec" label="Suec"/>
	<br>
	<br>
	<br>
	Edat del tripulant: <form:input path="edat"/>
	<form:errors path="edat" style="color:red"></form:errors> 
	<br>
	<br>
	<!--  EMAIL  -->
	Email del tripulant: <form:input path="email"/>
	<form:errors path="email" style="color:red"></form:errors> 
	<br>
	<br>
	<!-- PATTERN -->
	Codi postal: <form:input path="codiPostal"/>
	<form:errors path="codiPostal" style="color:red"></form:errors> 
	<br>
	<br>
	<!-- Telefon finalandia -->
	Telèfon: <form:input path="telefon"/>
	<form:errors path="telefon" style="color:red"></form:errors> 
	<br>
	<br>
	<!-- SUBMIT -->
	<input type="submit" value="Donar d'alta"/>
	</form:form>
</body>
</html>