# Access a dades  
  
`ORM` -> Mapeig a objecte relacional  
  
## HQL  
SQL propi de Hibernate, transformat automaticament al sistema de gestió de BBDD.  
  
## Session  
Codi que permet conectarse a BBDD, permet fer un `save` o un `get` per inserir o obtenir informació a la BBDD respectivament.  
  
```
Cliente client_1 = new Cliente("Pepito", "de los Palotes", "123456789");
int id = (Integer) session.save(client_1);

// esta fent v
INSERT INTO Cliente (nombre, apellido, tfno)VALUES ("Pepito", "de los Palotes", "123456789"

Cliente client_2 = session.get(Cliente.class, id);

```  
  
## Requeriments per a Hibernate:  
* XAMPP
* JDBC driver
* Java 8/11  
  
### pwd attempt at FTP server  
pass => 'admin'  
  

