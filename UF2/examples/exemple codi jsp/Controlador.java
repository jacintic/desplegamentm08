
@Controller
public class Controlador {
	List<Tripulant> llistaTripulants;
	List<Departament> llistaDepartaments;
	

	@RequestMapping("/")
	public String inici() {
		llistaTripulants = usAplicacio.inicialitzarTripulants();
		llistaDepartaments = usAplicacio.inicialitzarDepartaments_v2();

		return "inici";
	}
	
	
	
	@RequestMapping("/procesarLogin")
	public String procesarLogin(HttpServletRequest request, Model model) {
		boolean trobat = false;
		String dni;
		Tripulant tripulantTrobat;
		
		
		tripulantTrobat = new Tripulant();
		
		dni = request.getParameter("dni");
		
		for(Tripulant tripulantTmp : llistaTripulants) {
			if (dni.equalsIgnoreCase(tripulantTmp.getDni())) {
				trobat = true;
				tripulantTrobat = tripulantTmp;
				model.addAttribute("existeixLogin", dni);
				
				System.out.println("procesarLogin: trobat DNI");
			}
		}
		
		if (trobat == true) {
			model.addAttribute("tripulantTrobat", tripulantTrobat);
			model.addAttribute("llistaDepartaments", llistaDepartaments);
			
			return "modificarDadesTripulant";	
		}else {
			model.addAttribute("error", "ERROR, no existeix aquest DNI.");
			
			return "login";
		}
	}

	
}
